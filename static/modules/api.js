const getQuery = params => {
    if (!params) return ""

    const usp = new URLSearchParams()
    if (typeof params === "string") {
        usp.append("query", params)
    } else {
        usp.append("query", JSON.stringify(params))
    }
    return usp.toString()
}

export default (name, method, params) => {
    if (method === "GET") {
        return fetch(`/api/${name}?${getQuery(params)}`).then(res => res.json())
    } else {
        return fetch(
            `/api/${name}`,
            {
                method: "POST",
                headers: {"content-type": "application/json"},
                body: params ? JSON.stringify(params) : "",
            }
        ).then(res => res.json())
    }
}
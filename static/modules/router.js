const routes = [
	["/", "items"],
	["/auth", "auth"],
	["/register", "register"],
	[/^\/edit(\/(?<id>\d+))?$/, "edit"],
	[/.*/, "not-found"],
]

const getMatched = function*() {
	const path = location.pathname
	for (const [condition, module_name] of routes) {
		if (typeof condition === "string") {
			if (path === condition) yield { params: {}, module_name }
		} else if (condition instanceof RegExp) {
			const matched = path.match(condition)
			if (matched) yield { params: matched.groups, module_name }
		}
	}
}

for (const { module_name, params } of getMatched()) {
	const { render } = await import(`/modules/pages/${module_name}.js`)

	// If render function returns truthy value,
	// render the next matched module.
	if (!(await render(params))) {
		break
	}
}
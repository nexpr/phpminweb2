import { html, render as litRender } from "https://unpkg.com/lit-html@2.2.2/lit-html.js"
import api from "../api.js"
import css from "./items.css" assert { type: "css" }

let state = null
let root = null

const update = () => {
    litRender(template(state), root)
}

const action = (action, ...params) => async event => {
    await actions[action](params, event)
    update()
}

const actions = {
    async delete([id], event) {
        event.stopPropagation()
        if (!confirm("削除しますか？")) return

        const result = await api("item/delete", "POST", { id })
        if (result.success) {
            location.reload()
        } else {
            alert("削除に失敗しました")
        }
    },
    link([to, option]) {
        if (to === "edit") {
            const item = option
            if (!item) {
                location.href = `/edit`
            }
            if (item.user_id === state.me?.id) {
                location.href = `/edit/${item.id}`
            }
        } else {
            location.href = `/${to}`
        }
    },
    async logout() {
        const result = await api("logout")
        if (result.success) {
            location.reload()
        } else {
            alert("ログアウトに失敗しました")
        }
    },
}

const template = ({ items, me }) => html`
    <header class="right-btns">
        ${
            me
                ? html`<button @click=${action("logout")}>ログアウト</button>`
                : html`<button @click=${action("link", "auth")}>ログイン</button>`
        }
        ${
            me
                ? html`<button @click=${action("link", "edit")}>新規作成</button>`
                : null
        }
    </header>
    <table>
        <tbody>
            ${
                items.map(item => html`
                    <tr @click=${action("link", "edit", item)}>
                        <td>${item.id}</td>
                        <td>${item.body}</td>
                        <td>${item.username}</td>
                        <td>${new Date(item.ts).toLocaleString()}</td>
                        <td>
                            <button
                                .hidden=${item.user_id !== me?.id}
                                @click=${action("delete", item.id)}
                            >✕</button>
                        </td>
                    </tr>
                `)
            }
        </tbody>
    </table>
`

export const render = async () => {
    const [me, items] = await Promise.all([
        api("me", "GET"),
        api("items", "GET", decodeURIComponent(location.hash.slice(1)))
    ])
    state = { me, items}

    root = document.createElement("div")
    document.body.append(root)

    update()

    document.title = "List"
    document.adoptedStyleSheets = [css]
}

import api from "../api.js"
import css from "./register.css" assert { type: "css" }

const html = `
<main>
    <input id="username" placeholder="ユーザ名">
    <input id="password" type="password" placeholder="パスワード">
    <textarea id="permissions" placeholder="パーミッションリスト"></textarea>
    <button id="register">Register</button>
</main>
`

const register = async () => {
    const username = document.getElementById("username").value
    const password = document.getElementById("password").value
    const permissions = document.getElementById("permissions").value
        .split("\n").map(x => x.trim()).filter(x => x)

    const result = await api("register", "POST", { username, password, permissions })
    if (result.success) {
        location.href = "/auth"
    } else {
        alert("登録に失敗しました")
    }
}

export const render = async () => {
    document.body.innerHTML = html
    document.getElementById("register").onclick = register

    document.title = "Register"
    document.adoptedStyleSheets = [css]
}

import api from "../api.js"
import css from "./auth.css" assert { type: "css" }

const html = `
<main>
    <input id="username" placeholder="ユーザ名">
    <input id="password" type="password" placeholder="パスワード">
    <button id="login">Login</button>
</main>
`

const login = async () => {
    const username = document.getElementById("username").value
    const password = document.getElementById("password").value

    const result = await api("login", "POST", { username, password })
    if (result.success) {
        location.href = "/"
    } else {
        alert("ログインに失敗しました")
    }
}

export const render = async () => {
    document.body.innerHTML = html
    document.getElementById("login").onclick = login

    document.title = "Login"
    document.adoptedStyleSheets = [css]
}

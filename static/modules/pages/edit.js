import { html, render as litRender } from "https://unpkg.com/lit-html@2.2.2/lit-html.js"
import api from "../api.js"
import css from "./edit.css" assert { type: "css" }

let state = null
let root = null

const update = () => {
    litRender(template(state), root)
}

const action = (action, ...params) => async event => {
    await actions[action](params, event)
    update()
}

const actions = {
    input_body(params, event) {
        state.form.body = event.target.value
    },
    async save() {
        if (state.form.body) {
            const result = await api("item/save", "POST", { id: state.item?.id, body: state.form.body })
            if (result.success) {
                location.href = "/"
            } else {
                alert("保存に失敗しました")
            }
        } else {
            alert("入力してください")
        }
    },
}

const template = ({ form, item, me }) => html`
    <header>
        ${
            item
                ? html`
                    <h1>${item.id}</h1>
                    <span>${new Date(item.ts).toLocaleString()}</span>
                `
                : html`
                    <h1>New</h1>
                `
        }
    </header>
    <input
        .disabled=${item && item.user_id !== me.id}
        .value=${form.body}
        @change=${action("input_body")}
    >
    <div class="right-btns">
        <button @click=${action("save")} .disabled=${item && item.user_id !== me.id}>保存</button>
    </div>
`

export const render = async ({ id }) => {
    const me = await api("me", "GET")

    if (!me) {
        location.href = "/"
        return
    }

    if (id) {
        const item = await api("item", "GET", id)
        state = { me, item, form: { body: item.body }}
    } else {
        state = { me, form: { body: "" }}
    }

    root = document.createElement("div")
    document.body.append(root)

    update()

    document.title = "Edit"
    document.adoptedStyleSheets = [css]
}

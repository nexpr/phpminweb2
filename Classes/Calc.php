<?php

namespace phpminweb2\Classes;

class Calc {
    public int $value;

    function __construct(int $initial) {
        $this->value = $initial;
    }

    function add($value) {
        $this->value = $this->value + $value;
    }

    function sub($value) {
        $this->value = $this->value - $value;
    }

    function get() {
        return $this->value;
    }
}

<?php

namespace phpminweb2\Classes;

class User {
    function __construct(private $db) {
    }

    function authenticate($username, $password) {
        $rows = $this->db->query('
            SELECT id, hashed_password FROM user WHERE username = ?
        ', [$username]);

        $user = $rows[0] ?? null;

        if (!$user || !password_verify($password, $user->hashed_password)) {
            return null;
        }

        return $user->id;
    }

    function register($username, $password, $permissions){
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        $this->db->tx(function () use ($username, $hashed_password, $permissions) {
            $this->db->query('
                insert into user (username, hashed_password) values (?, ?)
            ', [$username, $hashed_password]);

            $id = $this->db->getLastId();

            if (is_array($permissions) && count($permissions) > 0) {
                $params = array_map(fn($permission) => [$id, $permission], $permissions);
                $query_placeholder = join(',', array_map(fn($x) => '(?,?)', $params));
                $params_flat = array_reduce($params, fn($a, $b) => array_merge($a, $b), []);
                $this->db->query('
                    insert into user_permission (user_id, permission)
                    values ' . $query_placeholder . '
                ', $params_flat);
            }
        });
    }

    function getById($id) {
        $rows = $this->db->query('
            SELECT * FROM user WHERE id = ?
        ', [$id]);

        $user = $rows[0] ?? null;
        if (!$user) return;

        $rows = $this->db->query('
            SELECT permission FROM user_permission WHERE user_id = ?
        ', [$user->id]);

        $user->permissions = array_map(fn($a) => $a->permission, $rows);

        return $user;
    }
}

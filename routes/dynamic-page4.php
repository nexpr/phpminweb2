<?php

use phpminweb2\Utils\Csession;
use phpminweb2\Utils\Response;

$sess = Csession::get();

$count = $sess->count ?? 0;
$sess->count = $count + 1;

Csession::set($sess);

Response::template('page4', $count);

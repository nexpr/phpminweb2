<?php

use phpminweb2\Utils\Auth;
use phpminweb2\Utils\Db;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\User;

$validated = Request::validate([
    'method' => 'POST',
]);

$auth = new Auth(new Db());
$auth->logout();

Response::json(['success' => true]);

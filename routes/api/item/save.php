<?php

use phpminweb2\Utils\Auth;
use phpminweb2\Utils\Db;
use phpminweb2\Utils\Permission;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\Item;
use phpminweb2\Classes\User;

$db = new Db();
$auth = new Auth(new User($db));
$user = $auth->requirePermission();

$validated = Request::validate([
    'method' => 'POST',
    'body' => [
        ['object', [
            'keys' => [
                'id' => [['null'], ['int', ['min' => 1]]],
                'body' => ['string', ['min' => 1]],
            ],
        ]],
    ],
]);

$item_id = $validated->body->id;
$body = $validated->body->body;

$item = new Item($db);

if ($item_id) {
    $item->update($item_id, $body, $user->id);
} else {
    $item->insert($body, $user->id);
}

Response::json(['success' => true]);

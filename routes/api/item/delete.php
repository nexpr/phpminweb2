<?php

use phpminweb2\Utils\Auth;
use phpminweb2\Utils\Db;
use phpminweb2\Utils\Permission;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\Item;
use phpminweb2\Classes\User;

$db = new Db();
$auth = new Auth(new User($db));
$user = $auth->requirePermission();

$validated = Request::validate([
    'method' => 'POST',
    'body' => [
        ['object', [
            'keys' => [
                'id' => ['int', ['min' => 1]],
            ],
        ]],
    ],
]);

$item_id = $validated->body->id;

$item = new Item($db);
$item->delete($item_id, $user->id);

Response::json(['success' => true]);

<?php

use phpminweb2\Utils\Db;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\User;

$validated = Request::validate([
    'method' => 'POST',
    'body' => ['object', [
        'keys' => [
            'username' => ['string'],
            'password' => ['string'],
            'permissions' => ['array', ['values' => ['string']]],
        ],
    ]],
]);

$username = $validated->body->username;
$password = $validated->body->password;
$permissions = $validated->body->permissions;

$user = new User(new Db());
$user->register($username, $password, $permissions);

Response::json(['success' => true]);

<?php

use phpminweb2\Utils\Auth;
use phpminweb2\Utils\Db;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\User;

$validated = Request::validate([
    'method' => 'GET',
]);

$auth = new Auth(new User(new Db()));
$user = $auth->getAuthenticated();

Response::json($user ? ['id' => $user->id, 'username' => $user->username] : null);

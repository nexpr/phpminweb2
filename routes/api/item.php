<?php

use phpminweb2\Utils\Db;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\Item;

$validated = Request::validate([
    'method' => 'GET',
    'query' => ['int', ['min' => 1]],
]);

$db = new Db();
$item = new Item($db);
$result = $item->byId($validated->query);

Response::json($result);

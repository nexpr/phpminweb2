<?php

use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;

$validated = Request::validate([
    'method' => 'GET',
    'query' => ['object', [
        'keys' => [
            'a' => ['int'],
            'b' => ['string'],
        ],
    ]],
]);

Response::json([
    'a' => $validated->query->a,
    'b' => $validated->query->b,
]);

<?php

use phpminweb2\Utils\Auth;
use phpminweb2\Utils\Db;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\User;

$validated = Request::validate([
    'method' => 'POST',
    'body' => ['object', [
        'keys' => [
            'username' => ['string'],
            'password' => ['string'],
        ],
    ]],
]);

$username = $validated->body->username;
$password = $validated->body->password;

$auth = new Auth(new User(new Db()));
$success = $auth->login($username, $password);

Response::json(['success' => $success]);

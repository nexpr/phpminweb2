<?php

use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;

$validated = Request::validate([
    'method' => 'POST',
    'query' => ['object', [
        'keys' => [
            'a' => ['int'],
            'b' => ['object', [
                'keys' => [
                    'c' => ['int'],
                ],
            ]],
        ],
    ]],
    'body' => ['object', [
        'allow_extra' => true,
        'keep_extra' => true,
        'keys' => [
            'prop1' => ['int'],
            'prop2' => ['int', ['convert' => false]],
            'prop3' => ['object', [
                'keys' => [
                    'foo' => ['null'],
                    'bar' => ['eq', ['values' => [1, 2, 3]]],
                ],
            ]],
            'prop4' => [
                ['string', ['min' => 1]],
                ['null'],
            ],
            'prop5' => ['array', [
                'values' => [
                    ['boolean'],
                    ['string'],
                ],
                'min' => 1,
                'unique' => true,
            ]],
            'prop6' => function($v, $value) {
                return $value === 1 ? $v->valid($value) : $v->invalid(['message' => 'not 1']);
            },
        ],
    ]],
]);

Response::json([
    'a' => $validated->query,
    'b' => $validated->body,
]);

/*
const query = encodeURIComponent(JSON.stringify({
    a: 1,
    b: {c: 3},
}))
const body = JSON.stringify({
    prop1: 1,
    prop2: 2,
    prop3: {foo: null, bar: 1},
    prop4: "a",
    prop5: [true, "a", false, "bc"],
    prop6: 1,
})
const ret = await fetch("/api/api3?query=" + query, {
    method: "POST",
    body,
    headers: {"content-type":"application/json"}
}).then(x => x.text())
console.log(ret)
*/
<?php

use phpminweb2\Utils\Db;
use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\Item;

$validated = Request::validate([
    'method' => 'GET',
    'query' => [
        ['null'],
        ['object', [
            'keys' => [
                'text' => [['null'], ['string', ['min' => 1]]],
                'from' => [['null'], ['date']],
                'to' => [['null'], ['date']],
                'user_id' => [['null'], ['int', ['min' => 1]]],
            ],
        ]],
    ],
]);

$db = new Db();
$item = new Item($db);
$result = $item->select($validated->query);

Response::json($result);

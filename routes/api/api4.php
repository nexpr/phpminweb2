<?php

use phpminweb2\Utils\Request;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\Calc;

$validated = Request::validate([
    'method' => 'GET',
    'query' => ['array', [
        'values' => ['object', [
            'keys' => [
                'type' => ['eq', ['values' => ['add', 'sub']]],
                'value' => ['int', ['min' => 1]],
            ],
        ]],
    ]],
]);

$c = new Calc(0);
foreach($validated->query as $v) {
    if ($v->type === 'add') {
        $c->add($v->value);
    } else if ($v->type === 'sub') {
        $c->sub($v->value);
    }
}

$result = $c->get();

Response::json($result);

/*
/api/api4?query=[{"type":"add","value":1},{"type":"sub","value":3}]

=> -2
*/
<?php

use phpminweb2\Utils\Auth;
use phpminweb2\Utils\Db;
use phpminweb2\Utils\Response;
use phpminweb2\Classes\User;

$auth = new Auth(new User(new Db()));
$auth->requirePermission('page3');

Response::template('page3', [
    'a' => 10,
    'b' => 20,
]);

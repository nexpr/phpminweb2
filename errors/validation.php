<?php

use phpminweb2\Utils\Response;

Response::html('
    <h1>Validation error</h1>
    <pre>' . json_encode($ex->detail) .'</pre>
', 400);

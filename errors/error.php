<?php

use phpminweb2\Utils\Response;

Response::html('
    <h1>Error</h1>
    <pre>' . $ex .'</pre>
', 500);

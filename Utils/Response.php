<?php

namespace phpminweb2\Utils;

class Response {
    static function html($html, $status = 200) {
        http_response_code($status);
        header('Content-Type: text/html');
        echo $html;
    }

    static function template($template, $value = [], $status = 200) {
        http_response_code($status);
        header('Content-Type: text/html');
        require Constant::get()->templates_dir . '/' . $template . '.php';
    }

    static function json($value, $status = 200) {
        http_response_code($status);
        header('Content-Type: application/json');
        echo json_encode($value);
    }

    static function file($filepath, $status = 200) {
        http_response_code($status);
        readfile($filepath);
    }
}

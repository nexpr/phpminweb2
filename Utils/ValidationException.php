<?php

namespace phpminweb2\Utils;

class ValidationException extends \Exception {
    function __construct(public $type, public $detail) {
        parent::__construct("validation error: $type");
    }
}

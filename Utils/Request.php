<?php

namespace phpminweb2\Utils;

class Request {
    static function validate($option) {
        if ($option['method'] ?? null !== null) {
            self::validateMethod($option['method']);
        }
        if ($option['csrf'] ?? true !== false) {
            self::validateCSRF();
        }

        $validated = new \stdClass();

        if ($option['query'] ?? null) {
            $validated->query = self::validateQuery($option['query']);
        }
        if ($option['body'] ?? null) {
            $validated->body = self::validateBody($option['body']);
        }

        return $validated;
    }

    static function validateMethod($required_method) {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method !== $required_method) {
            throw new ValidationException('method', ['method' => $method]);
        }
    }

    static function validateCSRF() {
        $headers = getallheaders();
        if (!in_array($headers['Sec-Fetch-Site'], ['same-origin', 'none'])) {
            throw new ValidationException('csrf', ['headers' => $headers]);
        }
    }

    static function validateQuery($option) {
        $json = $_GET['query'] ?? null;
        $query = json_decode($json);

        $validator = new ObjectValidator($query);
        $result = $validator->validate($option);
        if (!$result['valid']) {
            throw new ValidationException('query', $result);
        }
        return $result['value'];
    }

    static function validateBody($option) {
        $json = file_get_contents('php://input');
        $body = json_decode($json);

        $validator = new ObjectValidator($body);
        $result = $validator->validate($option);
        if (!$result['valid']) {
            throw new ValidationException('body', $result);
        }
        return $result['value'];
    }
}

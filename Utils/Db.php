<?php

namespace phpminweb2\Utils;

class Db {
    public \PDO $pdo;

    function __construct($config = null) {
        if (!$config) {
            $config = Env::get()->db;
        }
        $this->pdo = new \PDO(
            $config->dsn,
            $config->username,
            $config->password
        );
    }

    function query($sql, $params = []) {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    function getLastId() {
        return $this->pdo->lastInsertId();
    }

    function tx($fn) {
        $this->pdo->beginTransaction();
        try {
            $fn();
            $this->pdo->commit();
        } finally {
            if ($this->pdo->inTransaction()) {
                $this->pdo->rollBack();
            }
        }
    }
}

<?php

namespace phpminweb2\Utils;

class Auth {
    private $authenticated;

    function __construct(private $user) {
    }

    function login($username, $password) {
        $id = $this->user->authenticate($username, $password);
        if (!$id) {
            return false;
        }

        $sess = Csession::get();
        $sess->user = $id;
        Csession::set($sess);

        return true;
    }

    function logout() {
        $sess = Csession::get();
        $sess->user = null;
        Csession::set($sess);
    }

    function getAuthenticated() {
        if ($this->authenticated) {
            return $this->authenticated;
        } else if ($this->authenticated === false) {
            return null;
        } else {
            $id = Csession::get()->user ?? null;
            $this->authenticated = $id
                ? $this->user->getById($id) ?? false
                : false;
            return $this->getAuthenticated();
        }
    }

    function requirePermission($permission = null) {
        $user = $this->getAuthenticated();
        if (!$user || ($permission && !in_array($permission, $user->permissions, true))) {
            throw new PermissionException();
        }
        return $user;
    }
}

<?php

namespace phpminweb2\Utils;

class ObjectValidator {
    private $target;
    private $path;

    function __construct($target, $path = []) {
        $this->target = $target;
        $this->path = $path;
    }

    function validate($option) {
        if (is_callable($option)) {
            return $option($this, $this->target);
        }
        if (is_array($option)) {
            if (count($option) > 0 && is_string($option[0])) {
                $method = $option[0];
                $param = $option[1] ?? null;
                return $this->$method($param);    
            } else {
                $errors = [];
                foreach($option as $sub) {
                    $result = $this->validate($sub);
                    if ($result['valid']) {
                        return $result;
                    }
                    $errors[] = $result;
                }

                return $this->invalid([
                    'message' => 'all OR options failed',
                    'errors' => $errors,
                ]);
            }
        }
        throw new \Exception('invalid validation rule');
    }

    function int($option) {
        $value = $this->target;
        if (!is_int($value)) {
            if ($option['convert'] ?? true === false) {
                return $this->invalid(['message' => 'target is not int']);
            }
            if (is_numeric($value)) {
                $value = (int)$value;
            }else {
                return $this->invalid(['message' => 'cannot convert to int']);
            }
        }
        if ($option['min'] ?? null !== null) {
            if ($value < $option['min']) {
                return $this->invalid(['message' => 'not match min option']);
            }
        }
        if ($option['max'] ?? null !== null) {
            if ($value > $option['max']) {
                return $this->invalid(['message' => 'not match max option']);
            }
        }
        return $this->valid($value);
    }

    function float($option) {
        $value = $this->target;
        if (!is_float($value)) {
            if ($option['convert'] ?? true === false) {
                return $this->invalid(['message' => 'target is not float']);
            }
            if (is_numeric($value)) {
                $value = (float)$value;
            }else {
                return $this->invalid(['message' => 'cannot convert to float']);
            }
        }
        if ($option['min'] ?? null !== null) {
            if ($value < $option['min']) {
                return $this->invalid(['message' => 'not match min option']);
            }
        }
        if ($option['max'] ?? null !== null) {
            if ($value > $option['max']) {
                return $this->invalid(['message' => 'not match max option']);
            }
        }
        return $this->valid($value);
    }

    function boolean($option) {
        $value = $this->target;
        if (!is_bool($value)) {
            if ($option['convert'] ?? true === false) {
                return $this->invalid(['message' => 'target is not boolean']);
            }
            if (is_int($value) || is_float($value) || is_string($value) || is_null($value)) {
                $value = (boolean)$value;
            }else {
                return $this->invalid(['message' => 'cannot convert to boolean']);
            }
        }
        return $this->valid($value);
    }

    function string($option) {
        $value = $this->target;
        if (!is_string($value)) {
            if ($option['convert'] ?? true === false) {
                return $this->invalid(['message' => 'target is not string']);
            }
            if (is_int($value) || is_float($value)) {
                $value = (string)$value;
            }else {
                return $this->invalid(['message' => 'cannot convert to string']);
            }
        }
        if ($option['min'] ?? null !== null) {
            if (strlen($value) < $option['min']) {
                return $this->invalid(['message' => 'not match min option']);
            }
        }
        if ($option['max'] ?? null !== null) {
            if (strlen($value) > $option['max']) {
                return $this->invalid(['message' => 'not match max option']);
            }
        }
        return $this->valid($value);
    }

    function null() {
        if ($this->target === null) {
            return $this->valid(null);
        } else {
            return $this->invalid(['message' => 'value is not null']);
        }
    }

    function array($option) {
        $value = $this->target;
        if (!is_array($value)) {
            return $this->invalid(['message' => 'target is not array']);
        }
        if ($option['min'] ?? null !== null) {
            if (count($value) < $option['min']) {
                return $this->invalid(['message' => 'not match min option']);
            }
        }
        if ($option['max'] ?? null !== null) {
            if (count($value) > $option['max']) {
                return $this->invalid(['message' => 'not match max option']);
            }
        }
        if ($option['values'] ?? null !== null) {
            $new_value = [];
            foreach($value as $index => $item) {
                $path = $this->path;
                $path[] = $index;
                $sub = new self($item, $path);
                $subresult = $sub->validate($option['values']);
                if ($subresult['valid']) {
                    $new_value[] = $subresult['value'];
                } else {
                    return $subresult;
                }
            }
            $value = $new_value;
        }
        return $this->valid($value);
    }

    function object($option) {
        $value = $this->target;
        if (!is_object($value)) {
            return $this->invalid(['message' => 'target is not object']);
        }
        if ($option['keys'] ?? null !== null) {
            $new_value = new \stdClass();
            foreach($option['keys'] as $k => $v) {
                $path = $this->path;
                $path[] = $k;
                $sub = new self($value->$k ?? null, $path);
                $subresult = $sub->validate($v);
                if ($subresult['valid']) {
                    $new_value->$k = $subresult['value'];
                } else {
                    return $subresult;
                }
            }

            foreach($value as $k => $v) {
                if (array_key_exists($k, $option['keys'])) {
                    continue;
                }
                if (!($option['allow_extra'] ?? false)) {
                    $this->invalid(['message' => 'not allowed extra key', 'key' => $k]);
                }
                if ($option['keep_extra'] ?? false) {
                    $new_value[$k] = $value[$k];
                }
            }

            $value = $new_value;

        }
        return $this->valid($value);
    }

    function eq($option) {
        if (!array_key_exists('values', $option)) {
            return $this->invalid(['message' => 'eq method requires values property']);
        }

        $values = is_array($option['values']) ? $option['values'] : [$option['values']];
        if (in_array($this->target, $values, true)) {
            return $this->valid($this->target);
        } else {
            return $this->invalid(['message' => 'no equal values', 'values' => $values]);
        }
    }

    function date($option) {
        if (is_int($this->target)) {
            $date = new \DateTime();
            $date->setTimestamp($this->target);
            return $this->valid($date);
        } else if (is_string($this->target)) {
            try {
                $date = new \DateTime($this->target);
                return $this->valid($date);
            } catch (\Exception) {
                return $this->invalid(['message' => 'invalid date: parse error']);
            }
        }
        return $this->invalid(['message' => 'invalid date: type error']);
    }

    function any() {
        return $this->valid($this->target);
    }

    function valid($value) {
        return [
            'valid' => true,
            'path' => $this->path,
            'target' => $this->target,
            'error' => null,
            'value' => $value,
        ];
    }

    function invalid($error) {
        return [
            'valid' => false,
            'path' => $this->path,
            'target' => $this->target,
            'error' => $error,
            'value' => null,
        ];
    }
}

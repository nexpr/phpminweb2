<?php

namespace phpminweb2\Utils;

class Env {
    static function get() {
        $env = getenv('PHPMINWEB_ENV');
        $dir = self::getEnvDir();
        $result = null;

        if ($env) {
            $result = self::readJSON("{$dir}/{$env}.json");
        }
        if (!$result) {
            $result = self::readJSON("{$dir}/default.json");
        }
        if (!$result) {
            throw new \Exception('Invalid Env');
        }

        return $result;
    }

    static function getEnvDir() {
        $dir = getenv('PHPMINWEB_ENV_DIR');
        if ($dir && is_dir($dir)) {
            return $dir;
        } else {
            return __DIR__ . '/../env';
        }
    }

    static function readJSON(string $filename) {
        $json = file_get_contents($filename);
        if (!$json) return null;
        return json_decode($json);
    }
}
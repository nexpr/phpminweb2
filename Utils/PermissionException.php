<?php

namespace phpminweb2\Utils;

class PermissionException extends \Exception {
    function __construct() {
        parent::__construct("permission error");
    }
}
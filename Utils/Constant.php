<?php

namespace phpminweb2\Utils;

class Constant {
    static function get() {
        return (object)[
            'api_dir' => __DIR__ . '/../api',
            'pages_dir' => __DIR__ . '/../pages',
            'templates_dir' => __DIR__ . '/../templates',
            'default_html' => __DIR__ . '/../resources/default.html',
        ];
    }
}

<?php

namespace phpminweb2\Utils;

class Csession {
    const method = 'aes-256-cbc';

    static function get() {
        $env = Env::get();
        $cookie = $env->csession_cookie;
        $key = $env->csession_key;

        $value = @$_COOKIE[$cookie];
        if (!$value) return new \stdClass();

        [$iv_b64, $encrypted] = explode('|', $value, 2);
        $iv = base64_decode($iv_b64);

        $json = openssl_decrypt($encrypted, self::method, $key, 0, $iv);
        if (!$json) return new \stdClass();

        $data = json_decode($json);

        return $data;
    }

    static function set($data) {
        $env = Env::get();
        $cookie = $env->csession_cookie;
        $key = $env->csession_key;
        $iv = openssl_random_pseudo_bytes(16);
        $encrypted = openssl_encrypt(json_encode($data), self::method, $key, 0, $iv);
        $value = base64_encode($iv) . '|' . $encrypted;
        setcookie($cookie, $value, 0, '/', '', false, true);
    }
}

drop database if exists phpminweb;
create database phpminweb;
use phpminweb;

create table user (
    id integer not null auto_increment primary key,
    username varchar(32) not null unique,
    hashed_password text not null
);

create table user_permission (
    user_id integer not null references user(id),
    permission varchar(32) not null,
    primary key (user_id, permission)
);

create table item (
    id integer not null auto_increment primary key,
    body varchar(255) not null,
    ts timestamp not null,
    user_id integer not null references user(id)
);

<?php

require __DIR__ . '/vendor/autoload.php';

(function () {
    $path = parse_url($_SERVER['REQUEST_URI'])['path'];
    $filepath = __DIR__ . '/routes/' . $path . '.php';
    if (file_exists($filepath)) {
        try {
            require $filepath;
        } catch (\phpminweb2\Utils\ValidationException $ex) {
            require __DIR__ . '/errors/validation.php';
        } catch (\phpminweb2\Utils\PermissionException $ex) {
            require __DIR__ . '/errors/permission.php';
        } catch (\Exception $ex) {
            require __DIR__ . '/errors/error.php';
        }
    } else {
        require __DIR__ . '/errors/not-found.php';
    }
})();
